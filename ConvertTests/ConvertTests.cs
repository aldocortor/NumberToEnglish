﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberToEnglish;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberToEnglish.Tests
{
    [TestClass()]
    public class ConvertTests
    {

        [TestMethod()]
        public void convert5IntoEnglish()
        {
            Assert.AreEqual("five", Convert.convertNumberToEnglish(5));
        }

        [TestMethod()]
        public void convert17IntoEnglish()
        {
            Assert.AreEqual("seventeen", Convert.convertNumberToEnglish(17));
        }

        [TestMethod()]
        public void convert26IntoEnglish()
        {
            Assert.AreEqual("twenty-six", Convert.convertNumberToEnglish(26));
        }

        [TestMethod()]
        public void convert34IntoEnglish()
        {
            Assert.AreEqual("thirty-four", Convert.convertNumberToEnglish(34));
        }

        [TestMethod()]
        public void convert48IntoEnglish()
        {
            Assert.AreEqual("forty-eight", Convert.convertNumberToEnglish(48));
        }

        [TestMethod()]
        public void convert55IntoEnglish()
        {
            Assert.AreEqual("fifty-five", Convert.convertNumberToEnglish(55));
        }

        [TestMethod()]
        public void convert82IntoEnglish()
        {
            Assert.AreEqual("eighty-two", Convert.convertNumberToEnglish(82));
        }

        [TestMethod()]
        public void convert99IntoEnglish()
        {
            Assert.AreEqual("ninety-nine", Convert.convertNumberToEnglish(99));
        }

        [TestMethod()]
        public void convert115IntoEnglish()
        {
            Assert.AreEqual("one hundred fifteen", Convert.convertNumberToEnglish(115));
        }

        [TestMethod()]
        public void convert217IntoEnglish()
        {
            Assert.AreEqual("two hundred seventeen", Convert.convertNumberToEnglish(217));
        }

        [TestMethod()]
        public void convert326IntoEnglish()
        {
            Assert.AreEqual("three hundred twenty-six", Convert.convertNumberToEnglish(326));
        }

        [TestMethod()]
        public void convert434IntoEnglish()
        {
            Assert.AreEqual("four hundred thirty-four", Convert.convertNumberToEnglish(434));
        }

        [TestMethod()]
        public void convert548IntoEnglish()
        {
            Assert.AreEqual("five hundred forty-eight", Convert.convertNumberToEnglish(548));
        }

        [TestMethod()]
        public void conver655IntoEnglish()
        {
            Assert.AreEqual("six hundred fifty-five", Convert.convertNumberToEnglish(655));
        }

        [TestMethod()]
        public void convert782IntoEnglish()
        {
            Assert.AreEqual("seven hundred eighty-two", Convert.convertNumberToEnglish(782));
        }

        [TestMethod()]
        public void convert700IntoEnglish()
        {
            Assert.AreEqual("seven hundred", Convert.convertNumberToEnglish(700));
        }

        [TestMethod()]
        public void convert832IntoEnglish()
        {
            Assert.AreEqual("eight hundred thirty-two", Convert.convertNumberToEnglish(832));
        }

        

        [TestMethod()]
        public void convert999IntoEnglish()
        {
            Assert.AreEqual("nine hundred ninety-nine", Convert.convertNumberToEnglish(999));
        }

        [TestMethod()]
        public void convert0IntoEnglish()
        {
            Assert.AreEqual("zero", Convert.convertNumberToEnglish(0));
        }

        [TestMethod()]
        public void convert999832IntoEnglish()
        {
            Assert.AreEqual("nine hundred ninety-nine thousand eight hundred thirty-two", Convert.convertNumberToEnglish(999832));
        }



        [TestMethod()]
        public void convert782832IntoEnglish()
        {
            Assert.AreEqual("seven hundred eighty-two thousand eight hundred thirty-two", Convert.convertNumberToEnglish(782832));
        }

        [TestMethod()]
        public void convert782999IntoEnglish()
        {
            Assert.AreEqual("seven hundred eighty-two thousand nine hundred ninety-nine", Convert.convertNumberToEnglish(782999));
        }

        [TestMethod()]
        public void convert782999832IntoEnglish()
        {
            Assert.AreEqual("seven hundred eighty-two million nine hundred ninety-nine thousand eight hundred thirty-two", Convert.convertNumberToEnglish(782999832));
        }



        [TestMethod()]
        public void convert999782832IntoEnglish()
        {
            Assert.AreEqual("nine hundred ninety-nine million seven hundred eighty-two thousand eight hundred thirty-two", Convert.convertNumberToEnglish(999782832));
        }

        [TestMethod()]
        public void convert832782999IntoEnglish()
        {
            Assert.AreEqual("eight hundred thirty-two million seven hundred eighty-two thousand nine hundred ninety-nine", Convert.convertNumberToEnglish(832782999));
        }

        [TestMethod()]
        public void convert1832782999IntoEnglish()
        {
            Assert.AreEqual("one billion eight hundred thirty-two million seven hundred eighty-two thousand nine hundred ninety-nine", Convert.convertNumberToEnglish(1832782999));
        }

        [TestMethod()]
        public void convertNegative1832782999IntoEnglish()
        {
            Assert.AreEqual("negative one billion eight hundred thirty-two million seven hundred eighty-two thousand nine hundred ninety-nine", Convert.convertNumberToEnglish(-1832782999));
        }

        //[TestMethod()]
        //public void convert135IntoEnglish()
        //{
        //    var convert = new Convert();
        //    Assert.AreEqual("one hundred thirty-five", convert.convertNumberToEnglish(135));
        //}

        //[TestMethod()]
        //public void convert379IntoEnglish()
        //{
        //    var convert = new Convert();
        //    Assert.AreEqual("three hundred seventy-nine", convert.convertNumberToEnglish(379));
        //}

        //[TestMethod()]
        //public void convert550IntoEnglish()
        //{
        //    var convert = new Convert();
        //    Assert.AreEqual("five hundred fifty", convert.convertNumberToEnglish(550));
        //}

        //[TestMethod()]
        //public void convert1001IntoEnglish()
        //{
        //    var convert = new Convert();
        //    Assert.AreEqual("one thousand and one", convert.convertNumberToEnglish(1001));
        //}

        //[TestMethod()]
        //public void convert3874IntoEnglish()
        //{
        //    var convert = new Convert();
        //    Assert.AreEqual("three thousand eight hundred seventy-four", convert.convertNumberToEnglish(3874));
        //}

        //[TestMethod()]
        //public void convert55789IntoEnglish()
        //{
        //    var convert = new Convert();
        //    Assert.AreEqual("fifty-five thousand, seven hundred eighty-nine", convert.convertNumberToEnglish(55789));
        //}

        //[TestMethod()]
        //public void convert98765IntoEnglish()
        //{
        //    var convert = new Convert();
        //    Assert.AreEqual("ninety-eight thousand, seven hundred sixty-five", convert.convertNumberToEnglish(98765));
        //}


        //[TestMethod()]
        //public void convert1234567IntoEnglish()
        //{
        //    var convert = new Convert();
        //    Assert.AreEqual("one million, two hundred thirty-four thousand, five hundred sixty-seven", convert.convertNumberToEnglish(1234567));
        //}

        //[TestMethod()]
        //public void convert76543210IntoEnglish()
        //{
        //    var convert = new Convert();
        //    Assert.AreEqual("seventy-six million, five hundred forty-three thousand, two hundred ten", convert.convertNumberToEnglish(76543210));
        //}
    }
}