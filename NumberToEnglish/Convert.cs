﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberToEnglish
{
    public class Convert
    {
        private static string[] units = new string[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
        private static string[] tens = new string[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
        private static int inputNumber;
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number: ");
            string input = Console.ReadLine();
            inputNumber = Int32.Parse(input);
            Console.WriteLine(convertNumberToEnglish(inputNumber));
            Console.WriteLine("Press enter to close...");
            Console.ReadLine();
        }

        public static string convertNumberToEnglish(int number)
        {
            string result = "";
            if (number < 0)
            {
                number *= -1;
                result += "negative";
            }
            if (number > 1000000000)
            {
                result += getSpace(result) + getIndiviualNumber(number / 1000000000) + " billion";
                number %= 1000000000;
            }
            if (number > 1000000)
            {
                result += getSpace(result) + getIndiviualNumber(number / 1000000) + " million";
                number %= 1000000;
            }
            if (number > 1000)
            {
                result += getSpace(result)+getIndiviualNumber(number / 1000) + " thousand";
                number %= 1000;
            }
            result += getSpace(result) + getIndiviualNumber(number);
            return result;
        }

        private static string getSpace(string result) {
            return (result.Length == 0) ? "" : " "; 
        }

        private static string  getIndiviualNumber(int number)
        {
            string result = "";
            if (number > 100)
            {
                result += units[number/100] + " hundred";
                if (number % 100 > 0)
                {
                    number %= 100;
                    result += " ";
                }
                else
                    return result;
            }
            if (number < 20)
                result += units[number];
            else
            {
                result += tens[number / 10];
                if (number % 10 > 0)
                    result +="-"+ units[number % 10];
            }
            return result;
        }
    }
}
